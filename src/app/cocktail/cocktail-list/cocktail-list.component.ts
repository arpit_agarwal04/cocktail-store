import { HttpClient } from '@angular/common/http';
import { Component, OnInit, HostListener } from '@angular/core';
import { map } from 'rxjs/operators'

@Component({
  selector: 'app-cocktail-list',
  templateUrl: './cocktail-list.component.html',
  styleUrls: ['./cocktail-list.component.scss']
})
export class CocktailListComponent implements OnInit {
  //variables
  headers = [
    { key: 'strDrink', value: 'Drink Name' },
    { key: 'strCategory', value: 'Drink Category' },
    { key: 'strGlass', value: 'Drink Glass' },
    { key: 'strIngredient1', value: 'Drink Ingredient' }
  ];
  drinks_data: any;
  drinks_data_copy: any;
  isError: boolean = false;
  errorMessage: string = "We are facing some network issue. Please try after sometime."
  sortDirection: string = '';
  previousSortableHeader: string = '';
  searchText: string = '';
  drinkCategory = [
    { id: 0, category: 'All' },
    { id: 1, category: 'Shot' },
    { id: 2, category: 'Cocktail' },
    { id: 3, category: 'Ordinary Drink' },
    { id: 4, category: 'Beer' },
    { id: 5, category: 'Other/unknown' },
    { id: 6, category: 'Punch / Party Drink' }
  ]
  drinkIngredient = [
    { id: 0, name: 'All' },
    { id: 1, name: 'Kahlua' },
    { id: 2, name: 'Orange Bitters' },
    { id: 3, name: 'Gin' },
    { id: 4, name: 'Irish Cream' },
    { id: 5, name: 'Champagne' },
    { id: 6, name: 'Scotch' },
    { id: 7, name: 'Whiskey' },
    { id: 8, name: '	Pineapple Juice' },
    { id: 9, name: 'Rum' },
    { id: 10, name: 'Corona' },
    { id: 11, name: 'Hot Chocolate' },
    { id: 12, name: 'Sweet Vermouth' },
    { id: 13, name: 'Midori Melon Liqueur' },
    { id: 14, name: 'Vodka' },
    { id: 15, name: 'Southern Comfort' },
    { id: 16, name: 'Everclear' },
    { id: 17, name: 'Brandy' }
  ]
  selectedCategory: any;
  selectedIngredient: any;
  isLoading: boolean = true;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.selectedCategory = this.drinkCategory[0].id;
    this.selectedIngredient = this.drinkIngredient[0].id;
    //method used to fetch cocktails
    this.http.get('https://www.thecocktaildb.com/api/json/v1/1/search.php?f=b').subscribe(res => {
      this.drinks_data = res;
      this.drinks_data = this.drinks_data.drinks;
      this.drinks_data_copy = this.drinks_data.slice();
      this.isLoading = false;
      this.isError = false;
    },
      error => {
        this.isLoading = false;
        this.isError = true;
      })
  }

  @HostListener('document:keydown.enter', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.onSearch();
  }

  //search method
  onSearch(): void {
    console.log("test")
    if (this.searchText.length === 0 && this.drinkCategory[this.selectedCategory].category === 'All' && this.drinkIngredient[this.selectedIngredient].name === 'All') {
      this.drinks_data = this.drinks_data_copy.slice();
      this.sortDirection = '';
    }
    else {
      let paramSearch = this.drinks_data_copy.slice();
      paramSearch = this.transform(paramSearch, this.searchText, 'strDrink');
      console.log(1)
      if (this.drinkCategory[this.selectedCategory].category !== 'All') {
        paramSearch = this.transform(paramSearch, this.drinkCategory[this.selectedCategory].category, 'strCategory');
      }
      if (this.drinkIngredient[this.selectedIngredient].name !== 'All') {
        paramSearch = this.transform(paramSearch, this.drinkIngredient[this.selectedIngredient].name, 'strIngredient1');
      }
      this.drinks_data = paramSearch;
      this.sortDirection = ''
    }
  }


  transform(items: any[], searchText: string, columnKey: string): any[] {
    if (!items && !searchText) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    const result = items.filter(x => x[columnKey].toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
    return result.length > 0 ? result : [-1];
  }

  //drpdown filter change
  filterUpdate(): void {
    this.onSearch();
  }

  //sort method
  onSort(key: string): void {
    const data_length = this.drinks_data.length - 1;
    if (this.previousSortableHeader === '' || this.previousSortableHeader !== key) {
      this.previousSortableHeader = key;
      this.sortDirection = 'asc';
    }
    else {
      this.previousSortableHeader = key;
      if (this.sortDirection === 'asc') {
        this.sortDirection = 'desc';
      }
      else {
        this.sortDirection = 'asc';
      }
    }
    this.mergeSort(this.drinks_data, 0, data_length, key, this.sortDirection);
  }

  //merge sort method
  mergeSort(items: [], l: number, r: number, key: string, order: string): any {
    if (l >= r) {
      return;
    }
    const m = Math.floor((l + r - 1) / 2);
    this.mergeSort(items, l, m, key, order);
    this.mergeSort(items, m + 1, r, key, order);
    this.merge(items, l, m, r, key, order);
    return items;
  }

  //merge method
  merge(items: any, l: number, m: number, r: number, key: string, order: string): void {
    const n1 = m - l + 1;
    const n2 = r - m;
    const leftArray = new Array(n1);
    const rightArray = new Array(n2);
    for (let i = 0; i < n1; i++) {
      leftArray[i] = items[l + i];
    }
    for (let j = 0; j < n2; j++) {
      rightArray[j] = items[m + 1 + j];
    }
    let i = 0, j = 0, k = l;
    if (order === 'asc') {
      while (i < n1 && j < n2) {
        if (leftArray[i][key] <= rightArray[j][key]) {
          items[k] = leftArray[i];
          i++;
        } else {
          items[k] = rightArray[j];
          j++;
        }
        k++;
      }
      while (i < n1) {
        items[k] = leftArray[i];
        i++;
        k++;
      }
      while (j < n2) {
        items[k] = rightArray[j];
        j++;
        k++;
      }
    } else {
      while (i < n1 && j < n2) {
        if (leftArray[i][key] >= rightArray[j][key]) {
          items[k] = leftArray[i];
          i++;
        } else {
          items[k] = rightArray[j];
          j++;
        }
        k++;
      }
      while (i < n1) {
        items[k] = leftArray[i];
        i++;
        k++;
      }
      while (j < n2) {
        items[k] = rightArray[j];
        j++;
        k++;
      }
    }
  }

}
